# Warranty Frontend

Aplicación Angular del Sistema de Control de Garantia.

## Configuración de proxy dentro de Link
Para poder usar e instalar paquetes con npm, se debe acceder a través de un servidor proxy y deshabilitar la seguridad ssl, con los siguientes comandos:
* `npm config set proxy=http://proxym.redlink.com.ar:3128`
* `npm config delete http-proxy` y `npm config delete https-proxy`: estos dos comandos eliminan las otras dos configuraciones de proxy, que son innecesarias si las centralizamos todas con `proxy`.
* `npm config set strict-ssl=false`

## Guía de instalación rápida
* Instalar última version estable de NodeJS para el gestor de instalacion de paquetes (https://nodejs.org/es/).
* Instalar Angular CLI `npm install -g @angular/cli` usando el administrador de paquetes de npm.
* Clonar el proyecto.
* Ubicarse en la carpeta warranty-app del proyecto y ejecutar el comando `npm install`

## Recomendaciones
* Emplear Visual Studio Code como editor de código, por su versatilidad con la codificacion de typescript y por su acoplamiento y facilidad con el manejo de git (https://code.visualstudio.com/)
