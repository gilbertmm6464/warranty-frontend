import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrxAccountComponent } from './trx-account.component';

describe('TrxAccountComponent', () => {
  let component: TrxAccountComponent;
  let fixture: ComponentFixture<TrxAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrxAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrxAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
