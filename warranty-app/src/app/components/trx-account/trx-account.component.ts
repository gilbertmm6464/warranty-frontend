import { Component, OnInit, ViewChild} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-trx-account',
  templateUrl: './trx-account.component.html',
  styleUrls: ['./trx-account.component.css']
})
export class TrxAccountComponent implements OnInit {

  displayedColumns: string[] = [ 'entidad', 'ente', 'canal', 'trx', 'idGarantia', 'moneda'];
  dataSource = new MatTableDataSource<TrxAccountElement>(TRX_ACCOUNT_DATA);

   @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  constructor() { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }

}

export interface TrxAccountElement {
  entidad: string;
  ente: string;
  canal: string;
  trx: string;
  idGarantia: string;
  moneda: string;
}

const TRX_ACCOUNT_DATA: TrxAccountElement[] = [
  { entidad: '0029', ente: '***', canal: '***', trx: '***', idGarantia: '00052029', moneda: '*'},
  { entidad: '0014', ente: '***', canal: '***', trx: '10', idGarantia: '00052029', moneda: '*'},
  { entidad: '0011', ente: '***', canal: '***', trx: '***', idGarantia: '00052029', moneda: '*'}
]