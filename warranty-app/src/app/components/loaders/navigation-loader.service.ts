import { Loader } from "src/app/core/models/loader.models";

/**
 * Loader a utilizarse para mostrar en carga las distintas rutas.
 */
export class NavigationLoader extends Loader {}
