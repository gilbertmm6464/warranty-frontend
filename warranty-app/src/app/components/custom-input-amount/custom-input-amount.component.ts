import { Component, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, FormControl } from '@angular/forms';
import { formatCurrency, formatNumber } from '@angular/common';

@Component({
  selector: 'app-custom-input-amount',
  templateUrl: './custom-input-amount.component.html',
  styleUrls: ['./custom-input-amount.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CustomInputAmountComponent),
      multi: true
    }
  ]
})
export class CustomInputAmountComponent implements ControlValueAccessor {

  private jsonString: string;
  private parseError: boolean;
  private data: any;

  constructor() { }
  
  _onChange: () => void;
  onChange: any = () => {};
  onTouch: any = () => {};
  val: String = "";
  valNumber: String = "";

  set value(val){
    console.log("Escribió1");
    
    if( val !== undefined && this.val !== val){
      this.val = val;
      this.onChange(val);
      this.onTouch(val);

      // [(ngModel)]="formWarrantyAccount.controls['warrantyAmount'].value | currency:'ARS':'symbol-narrow'"
      console.log("Valor antes de modificar:", this.val);
      this.val = formatCurrency(val, 'es-AR', '$', 'ARS', '1.2-2');

      this.valNumber = formatNumber(val, 'en-US', '1.2-2');

      this.valNumber = this.valNumber.replace('.', ' ');
      
      while((this.valNumber.indexOf(",")>0)){
        this.valNumber = this.valNumber.replace(',', '.');
      }
      
      this.valNumber = this.valNumber.replace(' ', ',');
      
      this.val = this.valNumber;

    }
  }

  writeValue(value: any) {
    console.log("Escribió");
    this.value = value;

  }

  registerOnChange(fn: any) {
    console.log("Escribió2");
    this.onChange = fn
  }

  registerOnTouched(fn: any) {
    console.log("Escribió3");
    this.onTouch = fn;
  }

  registerOnValidatorChange(fn:any) {
    this._onChange = fn;
  }

  



}
