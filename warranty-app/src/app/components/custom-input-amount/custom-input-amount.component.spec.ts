import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomInputAmountComponent } from './custom-input-amount.component';

describe('CustomInputAmountComponent', () => {
  let component: CustomInputAmountComponent;
  let fixture: ComponentFixture<CustomInputAmountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomInputAmountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomInputAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
