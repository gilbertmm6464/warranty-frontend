import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css']
})
export class ConfigurationComponent implements OnInit {

  displayedColumns: string[] = [ 'idGarantia', 'garantia', 'anticipo', 'al1On', 'al1Off',
    'al2On', 'al2Off', 'al3On', 'al3Off', 'al4On', 'al4Off', 'al5On', 'al5Off', 'al6On', 'al6Off',
    'baja', 'tipo'];
  dataSource = new MatTableDataSource<AccountElement>(ACCOUNT_DATA);

  constructor() { }

  ngOnInit() {
    
  }

}


export interface AccountElement {
  idGarantia: string;
  garantia: string;
  anticipo: string;
  al1On: string;
  al1Off: string;
  al2On: string;
  al2Off: string;
  al3On: string;
  al3Off: string;
  al4On: string;
  al4Off: string;
  al5On: string;
  al5Off: string;
  al6On: string;
  al6Off: string;
  baja: string;
  tipo: string;
}

const ACCOUNT_DATA: AccountElement[] = [
  { idGarantia: '00000001', garantia: '500000', anticipo: '0', al1On: '90', al1Off: '85', 
      al2On: '95', al2Off: '91', al3On: '99', al3Off: '96', al4On: '300', al4Off: '300',
      al5On: '300', al5Off: '300', al6On: '300', al6Off: '300', baja: 'N', tipo: 'PP'}
  ];


