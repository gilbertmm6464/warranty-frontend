
/**
 * Class that define the particularities of the constants used on formatting
 */

export const ZERO = "0";
export const MAX_DECIMAL = 2;

export class FormatSettings{

    public static MAX_DECIMAL_ON_VIEW = 2;
    // Size of the value field
    public static ID_WARRANTY_SIZE = 8;
    public static WARRANTY_AMOUNT_SIZE = 12;
    public static RETAINER_SIZE = 12;
    public static ALARM_SIZE = 5;
    public static ACCOUNT_TYPE_SIZE = 2;
    


}