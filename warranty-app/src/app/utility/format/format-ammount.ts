import { SystemJsNgModuleLoader } from "@angular/core";
import { formatNumber } from "@angular/common";



const ID_WARRANTY_SIZE = 8;
const WARRANTY_AMOUNT_SIZE = 12;
const RETAINER_SIZE = 12;
const ALARM_SIZE = 5;
const MAX_DECIMAL = 2;
const ZERO = "0";



export class FormatAmmount {


    constructor(){

    }

    /* La idea es colocar todas las validaciones a pasar 
    para que sea un número válido */
    static isValidAmmountField(value: string): boolean {
        /* Si tiene puntos  o comas de mas no se formatea el código */
        return (!(value.indexOf(".")>0)  && 
            !(value.indexOf(",", (value.indexOf(",")+1))>0));

    }

    /* 
    *  Method that should be called when a field of amount type loses the focus
    */
    static onBlurAmmountField(value: string): string {

        if(this.isValidAmmountField(value)){

            value = value.replace(',', '.');
            value = formatNumber(Number(value), 'en-US', '1.2-2');
            value = value.replace('.', ' ');
        
            while((value.indexOf(",")>0)){
              value = value.replace(',', '.');
            }
            
            value = value.replace(' ', ',');
        }
   
        return value;
    }

    /* 
    *  Method that should be called when a field of amount type gain the focus
    */
    static onFocusAmmountField(fieldValue : string): string{
        
        while((fieldValue.indexOf(".")>0)){
            fieldValue = fieldValue.replace(".","");
        }
    
        return fieldValue;
    }



}