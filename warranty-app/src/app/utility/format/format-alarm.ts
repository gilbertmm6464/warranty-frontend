import { ZERO, MAX_DECIMAL } from "./format-settings";
import { formatNumber, formatPercent } from "@angular/common";

export class FormatAlarm {


    constructor(){

    }

    /* La idea es colocar todas las validaciones a pasar 
    para que sea un número válido */
    static isValidAlarmField(value: string): boolean {
        /* Si tiene puntos  o comas de mas no se formatea el código */
        return (!(value.indexOf(",", (value.indexOf(",")+1))>0));
        

    }

    /* Remove zero to the left */
    static removeZeroLeft(value: string): string {
        
        while((value.indexOf("0") == 0) && (value.indexOf(",")!=1)) {
            value = value.substring(1, value.length);
        }
        
        return value;
    }

    /* Si la cantidad de decimales indicados difiere de la máxima cantidad 
    *  permitida, se completa con cero o se trunca de acuerda a la cantidad 
    *  máxima de decimales que se permiten visualmente */
   static formatDecimals(value: string): string {
        
        let fieldAux: string = ""
        let j: number = 0;

        while(j < MAX_DECIMAL) {

            if(value.charAt(value.indexOf(",")+MAX_DECIMAL-j)=='') {
                fieldAux = ZERO + fieldAux;
            } else {
                fieldAux = value.charAt(value.indexOf(",")+MAX_DECIMAL-j) + fieldAux;
            }
            j++;

        }

        return fieldAux;
    }

    /* 
    *  Method that should be called when a field of amount type loses the focus
    */
    static onBlurAlarmField(value: string): string {
        let fieldAux : string = "";
        let i : number = 0;
        if(this.isValidAlarmField(value)){

            value = value.replace(',', '.');
            value = formatPercent(Number(value)/100, 'en-US', '1.2-2');
            value = value.replace('.', ' ');
        
            while((value.indexOf(",")>0)){
              value = value.replace(',', '.');
            }
            
            value = value.replace(' ', ',');
        }
   
        return value;
    }

     /* 
    *  Method that should be called when a field of amount type gain the focus
    */
   static onFocusAlarmField(fieldValue : string): string{
        
        while((fieldValue.indexOf(".")>0)){
            fieldValue = fieldValue.replace(".","");
        }
        fieldValue = fieldValue.replace("%","");
        return fieldValue;
    }


}