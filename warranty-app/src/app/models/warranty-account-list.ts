import { Alarm } from "./alarm";
import { WarrantyAccount } from "./warranty-account";

/**
 * Model that represents the warranty account
 */
export class WarrantyAccountList {

    readPrevious : string;
    totalAccount : string;
    qtyAccountsRead : string;
    accounts: WarrantyAccount[];

}