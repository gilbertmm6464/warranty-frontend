import { Alarm } from "./alarm";

/**
 * Model that represents the warranty account
 */
export class WarrantyAccount {

    id : string;
    warrantyAmount : string;
    retainer : string;
    accountType : string;
    alarms: Alarm[];
    approved: string;

}

