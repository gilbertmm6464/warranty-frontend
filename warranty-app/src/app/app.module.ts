
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';

import { HttpClientModule } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatDividerModule } from '@angular/material/divider';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { ScrollDispatchModule } from '@angular/cdk/scrolling';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TrxAccountComponent } from './components/trx-account/trx-account.component';
import { ConfigurationComponent } from './components/configuration/configuration.component';


import { LoginComponent } from './views/login/login.component';

/* Imports */

import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


/* Material Desing */

import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatStepperModule } from '@angular/material/stepper';

/* Sevices */

/* Views */

/* Components */

/* Routes */

import { APP_ROUTING } from '../app/views/app.routes';
import { NotFoundComponent } from './views/notfound/notfound.component';
import { WarrantyAccountService } from './services/warranty-account.service';
import { NavegacionComponent } from './views/navegacion/navegacion.component';
import { HomeComponent } from './views/home/home.component';
import { WarrantyAccountsComponent } from './views/warranty-accounts/warranty-accounts.component';
import { AuthenticateService } from './client/services/authenticate.service';
import { PoolNotificationService, DefaultNotifierService, NOTIFIER_SERVICES } from './core/services/notification.service';
import { SnackMessagesService } from './views/services/messages/snack-messages.service';
import { AuthGuardService } from './views/services/guards/auth-guard.services';
import { NavigationLoader } from './components/loaders/navigation-loader.service';
import { WarrantyAccountComponent } from './views/warranty-accounts/warranty-account/warranty-account.component';
import { WarrantyAccountConfigComponent } from './views/warranty-account-config/warranty-account-config.component';
import { WarrantiesAccountListComponent } from './views/dialog/warranties-accounts-list/warranties-accounts-list.component';
import { CdkTableModule } from '@angular/cdk/table';

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeFrExtra from '@angular/common/locales/extra/fr';
import { CustomInputAmountComponent } from './components/custom-input-amount/custom-input-amount.component';
import { StatusMonitorComponent } from './views/status-monitor/status-monitor.component';
import { StatusAlarmComponent } from './views/status-alarm/status-alarm.component';

// Graficos
import { ChartsModule } from 'ng2-charts';
import { AlarmChartsComponent } from './components/alarm-charts/alarm-charts.component';

registerLocaleData(localeFr, 'es-AR', localeFrExtra);

@NgModule({
  declarations: [
    AppComponent,
    TrxAccountComponent, 
    ConfigurationComponent, 
    LoginComponent, NotFoundComponent, NavegacionComponent,
    HomeComponent,
    WarrantyAccountsComponent,
    WarrantyAccountComponent,
    WarrantyAccountConfigComponent,
    WarrantiesAccountListComponent,
    CustomInputAmountComponent,
    StatusMonitorComponent,
    StatusAlarmComponent,
    AlarmChartsComponent
  ],
  imports: [
    
    BrowserModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule, 
    MatInputModule,
    AppRoutingModule,
    MatMenuModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatCardModule,
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    MatDialogModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDividerModule,
    MatGridListModule,
    ScrollDispatchModule,
    MatCheckboxModule,
    MatProgressBarModule,
    MatStepperModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatNativeDateModule,


    CdkTableModule,




    ChartsModule,


    APP_ROUTING
    
  ],
  exports: [
    MatMenuModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatCardModule,
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    MatDialogModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDividerModule,
    MatGridListModule,
    MatIconModule
    
 
  ],
  providers: [  
    NavigationLoader,  
    WarrantyAccountService,
    AuthenticateService,
    AuthGuardService,
    PoolNotificationService,
    DefaultNotifierService,
    {
      provide: NOTIFIER_SERVICES,
      useExisting: DefaultNotifierService,
      multi: true
    },
    SnackMessagesService,
    {
      provide: DefaultNotifierService,
      useExisting: SnackMessagesService
    }
    
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    WarrantiesAccountListComponent
  ]
})

/*import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ViewsModule } from '@bo/views';
import { RouterModule } from '@angular/router';

/**
 * Módulo principal para bootstrapping, no debería cambiar mucho mas que esto.
 */
/*@NgModule({
  declarations: [AppComponent],
  imports: [RouterModule, ViewsModule],
  providers: [],
  bootstrap: [AppComponent]
})
*/
export class AppModule { }
