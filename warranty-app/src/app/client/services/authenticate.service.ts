import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { UserAuthenticate } from './models/user-authenticate.model';
import { ClientWrapper } from './models/client-wrapper.model';
import { tap } from 'rxjs/operators';


@Injectable()
export class AuthenticateService extends ClientWrapper {

    /**
     * Llave usada para guardar los datos de la sesión en `localStorage`.
     */
    static LS_SESION_KEY = 'sesion';

    /* Endpoint del servicio */
    endpoint = 'http://localhost:8080/warranties/users/authentication/';

    /* Options */
    httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };

  constructor(private http: HttpClient) {
    super();
    console.log("Authenticate Server Listo");
  }


  /**
   * La sesión actual, o null si no existe.
   */
  get currentSession(): any { /* TODO */
    return JSON.parse(localStorage.getItem(AuthenticateService.LS_SESION_KEY));
  }


  getQuery() {

    // EndPoint
    const url = `http://localhost:8080/warranties/users/authentication`;
    
    // Header
    const headers = new HttpHeaders({
      // 'Authorization': 'Bearer BQD5KHh2LdCZfers2kaoVoZUjx7H40mo0SDnlbJPYZCjrlRUzw4McbajIvO-ZekV4BYBBfMtIb5wx1M2Tpc'
    });

    return this.http.get(url, { headers });
    
  }

  LoginAuthenticate (userAuthenticate: UserAuthenticate){

    console.log(userAuthenticate);

    return this.loader.load(this.http.post('http://localhost:8080/warranties/users/authentication', 
                    JSON.stringify(userAuthenticate), this.httpOptions)).pipe(
                       tap((res: any) => {
                            console.log(res);
                            localStorage.setItem( AuthenticateService.LS_SESION_KEY, JSON.stringify(res) );
                        }
                        /*,
                        err => {
                            console.log("Error occured");
                            
                        }
                        */
                    ));
                
  }

  /**
   * Realiza el logout del usuario actual, borrando la sesión de `localStorage`.
   */
  logout() {
    localStorage.removeItem(AuthenticateService.LS_SESION_KEY);
  }

}
