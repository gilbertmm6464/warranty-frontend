import { Loader } from "src/app/core/models/loader.models";


/**
 * Abstracción que engloba a un cliente a ser utilizada para todas las llamadas al backend.
 */
export class ClientWrapper {
  /**
   * Loader que será instanciado al ser requerido.
   */
  private _loader: Loader;
  /**
   * Loader a ser utilizado para realizar todas las llamadas al backend.
   * Se instanciará la primera vez que sea requerido.
   */
  get loader(): Loader {
    if (!this._loader) {
      this._loader = new Loader();
    }
    return this._loader;
  }
}
