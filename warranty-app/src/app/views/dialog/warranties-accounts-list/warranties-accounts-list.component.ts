import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { WarrantyAccountList } from 'src/app/models/warranty-account-list';
import { WarrantyAccountService } from 'src/app/services/warranty-account.service';
import { WarrantyAccount } from 'src/app/models/warranty-account';

export interface Tile {
  text: string;
  cols: number;
  rows: number;
  valorOn: string;
  valorOff: string;
}

@Component({
  selector: 'app-warranties-accounts-list',
  templateUrl: './warranties-accounts-list.component.html',
  styleUrls: ['./warranties-accounts-list.component.css']
})
export class WarrantiesAccountListComponent implements OnInit {
  
  displayedColumns: string[] = [ 'select', 'id', 'garantia', 'anticipo', 'al1On', 'al1Off', 'al2On', 
  'al2Off', 'al3On', 'al3Off', 'al4On', 'al4Off', 'al5On', 'al5Off', 'al6On', 'al6Off', 'tipo'];
  dataSource = new MatTableDataSource<WarrantyAccount>();
  selection = new SelectionModel<WarrantyAccount>(true, []);
  visibleActionItems: boolean = false;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;


  constructor(private dialogRef: MatDialogRef<WarrantiesAccountListComponent>,
              private snackBar: MatSnackBar,
              private warrantyAccountService : WarrantyAccountService
              ) { 


  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    /* Consult Warranty Account Service - Get all warranty accounts */
    this.warrantyAccountService.getWarrantyAccounts('N', '00000000')
          .subscribe((data:WarrantyAccountList)=>{
      
      data.accounts.forEach(element => {
             this.dataSource.data.push(element);
           });
           
      console.log("Data"+JSON.stringify(this.dataSource.data));

      this.refreshTable();


    });

  }

  refreshTable(){
    /* TODO - no conforme */
    this.dataSource.filter = ' ';
    this.dataSource.filter = '';
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: WarrantyAccount): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  validateSelection(): void {

    let message = "No se puede seleccionar mas de una garantia";
    let accion = "Aceptar";
    let rowSelected: WarrantyAccount;

    if( this.selection.selected.length == 1 ) {
      console.log(" Cerrando dialogo ");
      this.dataSource.data.forEach(row => {

        if(this.selection.isSelected(row)){
          rowSelected = row;
        }

      });

      this.dialogRef.close(rowSelected);

    } else {
      console.log("No se puede cerrar el dialogo:"+this.selection.selected.length);
      this.snackBar.open(message, accion, {
        duration: 3000,
      });
    }
  }

}


