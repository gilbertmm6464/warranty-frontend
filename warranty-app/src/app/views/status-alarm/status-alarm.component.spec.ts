import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusAlarmComponent } from './status-alarm.component';

describe('StatusAlarmComponent', () => {
  let component: StatusAlarmComponent;
  let fixture: ComponentFixture<StatusAlarmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusAlarmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusAlarmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
