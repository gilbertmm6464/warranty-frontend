import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { WarrantyAccount } from 'src/app/models/warranty-account';
import { StatusAlarm } from 'src/app/models/status-alarm';

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

// Depending on whether rollup is used, moment needs to be imported differently.
// Since Moment.js doesn't have a default export, we normally need to import using the `* as`
// syntax. However, rollup creates a synthetic default module and we thus need to import it using
// the `default as` syntax.
// import * as _moment from 'moment';
// // // tslint:disable-next-line:no-duplicate-imports
// import {default as _rollupMoment} from 'moment';

// const moment = _rollupMoment || _moment;


@Component({
  selector: 'app-status-alarm',
  templateUrl: './status-alarm.component.html',
  styleUrls: ['./status-alarm.component.css'],
  providers:[
    // The locale would typically be provided on the root module of your application. We do it at
    // the component level here, due to limitations of our example generation script.
    {provide: MAT_DATE_LOCALE, useValue: 'es-AR'},

    // {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS}
  ]
})
export class StatusAlarmComponent implements OnInit {

  trxTypes: TrxType[] = [
    {trxTypeCode: '##', trxTypeViewValue: '## - Garantía Global'},
    {trxTypeCode: '**', trxTypeViewValue: '** - Cualquier transacción'},
    {trxTypeCode: '10', trxTypeViewValue: '10 - Transacciones'},
    {trxTypeCode: '19', trxTypeViewValue: '19 - Débitos por Transferencia Interbancaria'},
    {trxTypeCode: '81', trxTypeViewValue: '81 - Pagos de Automatico de Servicios'},
    {trxTypeCode: '88', trxTypeViewValue: '88 - Pagos de Servicios Sin Base'},
    {trxTypeCode: 'TR', trxTypeViewValue: 'TR - Transferencias'},
    {trxTypeCode: 'PG', trxTypeViewValue: 'PG - Pagos'},
    {trxTypeCode: 'EX', trxTypeViewValue: 'EX - Extracciones'}
  ];

  currencys: Currency[] = [
    {currencyCode: '#', currencyViewValue: '# - General'},
    {currencyCode: '*', currencyViewValue: '* - Todas las monedas'},
    {currencyCode: 'P', currencyViewValue: 'P - Pesos (032)'},
    {currencyCode: 'D', currencyViewValue: 'D - Dolares (840)'},
    {currencyCode: 'R', currencyViewValue: 'R - Uruguayos (858)'}
  ];

  displayedColumns: string[] = [ 'fechaGeneracion', 'diaNegocio', 'nivelAlarma', 'diaActual', 
    'diaSiguiente', 'dia1', 'dia2', 'dia3', 'dia4', 'dia5', 'dia6', 'dia7'];
  dataSource = new MatTableDataSource<StatusAlarm>();
  selection = new SelectionModel<StatusAlarm>(true, []);


  formMonitorAlarm: FormGroup;
  date = new FormControl();
  date2 = new FormControl();

  constructor() { }

  ngOnInit() {
    this.defineForm();
  }
  
  defineForm(){
    this.formMonitorAlarm = new FormGroup({
        
      'idWarrantyAccount': new FormControl('', [Validators.required,
                                                Validators.pattern("[0-9]{1,8}")
      ])

    })
  }

}

export interface TrxType {
  trxTypeCode: string;
  trxTypeViewValue: string;
}

export interface Currency {
  currencyCode: string;
  currencyViewValue: string;
}


