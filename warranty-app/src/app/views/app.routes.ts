import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './notfound/notfound.component';
import { NavegacionComponent } from './navegacion/navegacion.component';
import { HomeComponent } from './home/home.component';
import { WarrantyAccountsComponent } from './warranty-accounts/warranty-accounts.component';
import { AuthGuardService } from './services/guards/auth-guard.services';
import { WarrantyAccountComponent } from './warranty-accounts/warranty-account/warranty-account.component';
import { WarrantyAccountConfigComponent } from './warranty-account-config/warranty-account-config.component';
import { StatusMonitorComponent } from './status-monitor/status-monitor.component';
import { StatusAlarmComponent } from './status-alarm/status-alarm.component';

/*
const APP_ROUTES: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'menu', component: MenuComponent },
    { path: 'home', component: NavegacionComponent },
    { path: 'notfound', component: NotfoundComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'notfound' }
];
*/


const APP_ROUTES: Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: '',
        component: NavegacionComponent,
        canActivate: [AuthGuardService],
        canActivateChild: [AuthGuardService],
        children: [  
            { path: 'home', component: HomeComponent },
            { path: 'warranty-accounts', component: WarrantyAccountsComponent },
            { path: 'warranty-accounts/new', component: WarrantyAccountComponent },
            { path: 'warranty-accounts/edit/:id', component: WarrantyAccountComponent },
            { path: 'warranty-account-config', component: WarrantyAccountConfigComponent },
            { path: 'state-monitor', component: StatusMonitorComponent },
            { path: 'status-alarm', component: StatusAlarmComponent },
            { path: '**', pathMatch: 'full', redirectTo: 'home' }
        ]
    }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);

