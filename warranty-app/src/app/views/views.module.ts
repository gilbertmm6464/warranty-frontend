/*
import { NgModule } from '@angular/core';
import { RoutingModule } from './routing.module';
import { NavegacionComponent } from './navegacion/navegacion.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ComponentsModule } from '@components';
import { NotFoundComponent } from './notfound/notfound.component';
import { ClientModule } from '@client';
import { CoreModule, DefaultNotifierService } from '@core';
import { SnackMessagesService } from './services/messages/snack-messages.service';

*/

/**
 * Componente que incluye todas las vistas de la aplicación.
 */
/*
@NgModule({
  declarations: [
    NavegacionComponent,
    HomeComponent,
    LoginComponent,
    NotFoundComponent
  ],
  imports: [RoutingModule, ComponentsModule, CoreModule, ClientModule],
  providers: [
    SnackMessagesService,
    {
      provide: DefaultNotifierService,
      useExisting: SnackMessagesService
    },
  ]
})
export class ViewsModule {}
*/