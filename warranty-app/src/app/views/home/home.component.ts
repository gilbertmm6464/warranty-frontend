import { Component, OnInit, ViewChild } from '@angular/core';
import { WarrantyAccount } from 'src/app/models/warranty-account';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { WarrantyAccountList } from 'src/app/models/warranty-account-list';
import { WarrantyAccountService } from 'src/app/services/warranty-account.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  displayedColumns: string[] = [ 'select', 'id', 'garantia', 'anticipo', 'al1On', 'al1Off', 'al2On', 
  'al2Off', 'al3On', 'al3Off', 'al4On', 'al4Off', 'al5On', 'al5Off', 'al6On', 'al6Off', 'tipo'];
  warranties : WarrantyAccount[] = [];

  constructor(

  private warrantyAccountService : WarrantyAccountService) { }

  // @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    // this.dataSource.paginator = this.paginator;
    /* Consult Warranty Account Service - Get all warranty accounts */
    this.warrantyAccountService.getWarrantyAccounts('N', '00000000')
          .subscribe((data:WarrantyAccountList)=>{
      
      data.accounts.forEach(element => {
        this.warranties.push(element);
      });

      console.log("Data"+JSON.stringify(this.warranties));
      // this.refreshTable();
    });

  }

  // refreshTable(){
  //   /* TODO - no conforme */
  //   // this.dataSource.filter = ' ';
  //   // this.dataSource.filter = '';
  // }

}
