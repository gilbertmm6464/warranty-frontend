import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { WarrantiesAccountListComponent } from '../dialog/warranties-accounts-list/warranties-accounts-list.component';
import { WarrantyAccount } from 'src/app/models/warranty-account';

@Component({
  selector: 'app-warranty-account-config',
  templateUrl: './warranty-account-config.component.html',
  styleUrls: ['./warranty-account-config.component.css']
})
export class WarrantyAccountConfigComponent implements OnInit {

  displayedColumns: string[] = [ 'id', 'garantia', 'anticipo', 'al1On', 'al1Off', 'al2On', 
  'al2Off', 'al3On', 'al3Off', 'al4On', 'al4Off', 'al5On', 'al5Off', 'al6On', 'al6Off', 'tipo'];
  dataSource = new MatTableDataSource<WarrantyAccount>();
  trxTypes: TrxType[] = [
    {trxTypeCode: '##', trxTypeViewValue: '## - Garantía Global'},
    {trxTypeCode: '**', trxTypeViewValue: '** - Cualquier transacción'},
    {trxTypeCode: '10', trxTypeViewValue: '10 - Transacciones'},
    {trxTypeCode: '19', trxTypeViewValue: '19 - Débitos por Transferencia Interbancaria'},
    {trxTypeCode: '81', trxTypeViewValue: '81 - Pagos de Automatico de Servicios'},
    {trxTypeCode: '88', trxTypeViewValue: '88 - Pagos de Servicios Sin Base'},
    {trxTypeCode: 'TR', trxTypeViewValue: 'TR - Transferencias'},
    {trxTypeCode: 'PG', trxTypeViewValue: 'PG - Pagos'},
    {trxTypeCode: 'EX', trxTypeViewValue: 'EX - Extracciones'}
  ];

  currencys: Currency[] = [
    {currencyCode: '#', currencyViewValue: '# - General'},
    {currencyCode: '*', currencyViewValue: '* - Todas las monedas'},
    {currencyCode: 'P', currencyViewValue: 'P - Pesos (032)'},
    {currencyCode: 'D', currencyViewValue: 'D - Dolares (840)'},
    {currencyCode: 'R', currencyViewValue: 'R - Uruguayos (858)'}
  ];

  constructor(public dialog: MatDialog,
              private changeDetectorRefs: ChangeDetectorRef) { 
                
  }

  ngOnInit() {
    this.changeDetectorRefs.detectChanges();
  }

  selectedWarrantyAccount() {
    const dialogRef = this.dialog.open(WarrantiesAccountListComponent);

    dialogRef.afterClosed().subscribe(data => {

      this.dataSource.data.splice(0,1);
      this.dataSource.data.push(data);
      this.refreshTable();
      console.log(this.dataSource.data);
      console.log(`Dialog result: ${JSON.stringify(data)}`);

    });
  }

  refreshTable(){
    /* TODO - no conforme */
    this.dataSource.filter = ' ';
    this.dataSource.filter = '';
  }

}

export interface TrxType {
  trxTypeCode: string;
  trxTypeViewValue: string;
}

export interface Currency {
  currencyCode: string;
  currencyViewValue: string;
}


