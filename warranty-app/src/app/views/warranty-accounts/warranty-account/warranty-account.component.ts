import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { WarrantyAccount } from 'src/app/models/warranty-account';
import { stringToKeyValue } from '@angular/flex-layout/extended/typings/style/style-transforms';
import { WarrantyAccountService } from 'src/app/services/warranty-account.service';
import { Alarm } from 'src/app/models/alarm';
import { FormatAmmount } from 'src/app/utility/format/format-ammount';
import { FormatAlarm } from 'src/app/utility/format/format-alarm';

@Component({
  selector: 'app-warranty-account',
  templateUrl: './warranty-account.component.html',
  styleUrls: ['./warranty-account.component.css']
})
export class WarrantyAccountComponent implements OnInit {

  
  visible: boolean = false;
  formWarrantyAccount: FormGroup;
  warrantyAccount:  WarrantyAccount;
  isEditView: boolean;
  // idWarrantyAccount: String;


  accountTypes: AccountType[] = [
    {accountTypeCode: 'PP', accountTypeViewValue: 'Pesos'},
    {accountTypeCode: 'DD', accountTypeViewValue: 'Dolares'},
    {accountTypeCode: 'EE', accountTypeViewValue: 'Euros'}
  ];


  onToggleFab() {
    this.visible = !this.visible;
  }

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private warrantyAccountService : WarrantyAccountService) { 

  }

  ngOnInit() {

    console.log(this.warrantyAccount);
    this.defineForm();

    if (this.activatedRoute) {
      // if exist id in the URL(+) converts string 'id' to a number and search a SRM
      const id = this.activatedRoute.snapshot.params['id'];

      if (id != null){
        this.isEditView = true;
        // Llamada a servicio
        this.warrantyAccountService.getWarrantyAccount(id).subscribe((account:WarrantyAccount)=>{
        
          console.log("JSON recibido", JSON.stringify(account));
          // Set values of the warranties accounts
          this.formWarrantyAccount.controls['accountType'].setValue(account.accountType);
          this.formWarrantyAccount.controls['idWarrantyAccount'].setValue(account.id);
          this.formWarrantyAccount.controls['retainer'].setValue(account.retainer.slice(0,10)+","+
            account.retainer.slice(10,12));
          this.formWarrantyAccount.controls['warrantyAmount'].setValue(account.warrantyAmount.slice(0,10)+","+
            account.warrantyAmount.slice(10,12));

          account.alarms.forEach(alarm => {
            alarm.alOff = FormatAlarm.onFocusAlarmField(alarm.alOff);  
            alarm.alOff = FormatAlarm.onBlurAlarmField(alarm.alOff);  
            alarm.alOn = FormatAlarm.onFocusAlarmField(alarm.alOn);  
            alarm.alOn = FormatAlarm.onBlurAlarmField(alarm.alOn); 

          });
            
          // Set Alarms
          (<FormArray>this.formWarrantyAccount.controls['alarms']).setValue(account.alarms);
          
          this.onFocusWarrantyAmount();
          this.onBlurWarrantyAmount();
          this.onFocusRetainerAmount();
          this.onBlurRetainerAmount();



        });

        

      } else {
        console.log("NO Tiene id");
      }
      console.log("id: "+ id);

    }

  }

  saveWarrantyAccount() {

    // console.log("Formulario:"+JSON.stringify(form));
    this.warrantyAccount = new WarrantyAccount();
    console.log( "ngForm", this.formWarrantyAccount );
     
    // Set values of the warranties accounts

    this.warrantyAccount.accountType = this.formWarrantyAccount.controls['accountType'].value;
    this.warrantyAccount.id = this.formWarrantyAccount.controls['idWarrantyAccount'].value.toString();
    this.warrantyAccount.retainer = this.formWarrantyAccount.controls['retainer'].value;
    this.warrantyAccount.warrantyAmount = this.formWarrantyAccount.controls['warrantyAmount'].value;
    this.warrantyAccount.alarms = <Alarm[]>(<FormArray>this.formWarrantyAccount.controls['alarms']).getRawValue();
    this.warrantyAccount.approved = "N";   

    // Format y validated Data of the Warranty Account
    this.warrantyAccount = this.formatDataSend(this.warrantyAccount);

    

    console.log(JSON.stringify(this.warrantyAccount));
    console.log("isEditView = " + this.isEditView);

    // Call Warranty Account Service 
    if (!this.isEditView) {
      // Create Warranty Account
      console.log(this.warrantyAccount);
      this.warrantyAccountService.createWarrantyAccount(this.warrantyAccount)
              .subscribe(response => {
                console.log();

              }
              
      );
    } else {
      console.log('Guardar una modificación');
      // Update Warranty Account
      console.log(this.warrantyAccount);
      this.warrantyAccountService.updateWarrantyAccount(this.warrantyAccount)
              .subscribe(response => {
                console.log();
              });
    }
    
  }

  defineForm() {

    this.formWarrantyAccount = new FormGroup({
      
      'idWarrantyAccount': new FormControl('', [Validators.required,
                                                Validators.pattern("[0-9]{1,8}")
      ]),
      'warrantyAmount': new FormControl('', 
      //[Validators.required
        // ,
        //                                      Validators.maxLength(16),
        //                                      Validators.pattern("^[0-9]{1,3}(?:.[0-9]{3})*(?:\,[0-9]+)?$")                          
      //]
      ),
      'retainer': new FormControl('', [Validators.required]),
      'accountType': new FormControl('', [Validators.required]),
      
      'alarms': new FormArray([
        new FormGroup({
          'alOn': new FormControl('300', [Validators.required]),
          'alOff': new FormControl('300', [Validators.required])
        }),
        new FormGroup({
          'alOn': new FormControl('300', [Validators.required]),
          'alOff': new FormControl('300', [Validators.required])
        }),
        new FormGroup({
          'alOn': new FormControl('300', [Validators.required]),
          'alOff': new FormControl('300', [Validators.required])
        }),
        new FormGroup({
          'alOn': new FormControl('300', [Validators.required]),
          'alOff': new FormControl('300', [Validators.required])
        }),
        new FormGroup({
          'alOn': new FormControl('300', [Validators.required]),
          'alOff': new FormControl('300', [Validators.required])
        }),
        new FormGroup({
          'alOn': new FormControl('300', [Validators.required]),
          'alOff': new FormControl('300', [Validators.required])
        }),
      ])
    
    })

  }

  formatAlarmsDataSend(alarms: Alarm[]): Alarm[] {

    alarms.forEach(alarm => {
      while (alarm.alOff.length < ALARM_SIZE) {
        alarm.alOff = "0" + alarm.alOff;
      }

      while (alarm.alOn.length < ALARM_SIZE) {
        alarm.alOn = "0" + alarm.alOn;
      }
  
    });
   
    return alarms;
  }

  onBlurIdWarranty() {

      let fieldAux: string = this.formWarrantyAccount.controls['idWarrantyAccount'].value;
      let fieldSize : number = this.formWarrantyAccount.controls['idWarrantyAccount'].value.toString().length;

      while (fieldSize < ID_WARRANTY_SIZE){
        fieldAux = "0"+fieldAux;
        fieldSize += 1;
      }

      this.formWarrantyAccount.controls['idWarrantyAccount'].setValue(fieldAux);
  }

  onBlurWarrantyAmount() {
    
    let fieldValue : String = "";
    fieldValue = FormatAmmount.onBlurAmmountField(this.formWarrantyAccount.controls['warrantyAmount'].value.toString());

    this.formWarrantyAccount.controls['warrantyAmount'].setValue(fieldValue);
  }

  onFocusWarrantyAmount() {
    
    let fieldValue : String = "";
    fieldValue = FormatAmmount.onFocusAmmountField(this.formWarrantyAccount.controls['warrantyAmount'].value.toString());
       
    this.formWarrantyAccount.controls['warrantyAmount'].setValue(fieldValue);
  }

  onBlurRetainerAmount() {
    
    let fieldAux : String = ""
    fieldAux = FormatAmmount.onBlurAmmountField(this.formWarrantyAccount.controls['retainer'].value.toString());

    this.formWarrantyAccount.controls['retainer'].setValue(fieldAux);
  }

  onFocusRetainerAmount() {
    
    let fieldValue : String = "";
    fieldValue = FormatAmmount.onFocusAmmountField(this.formWarrantyAccount.controls['retainer'].value.toString());
       
    this.formWarrantyAccount.controls['retainer'].setValue(fieldValue);
  }

  onBlurAlarmOnField(position: number) {
    
    let fieldValue : string = "";
    let alarms: Alarm[] =  <Alarm[]>(<FormArray>this.formWarrantyAccount.controls['alarms']).getRawValue();
    fieldValue = FormatAlarm.onBlurAlarmField(alarms[position].alOn);  
    alarms[position].alOn = fieldValue;
  
    this.formWarrantyAccount.controls['alarms'].setValue(alarms); 
  }

  onFocusAlarmOnField(position: number) {
    let fieldValue : string = "";
    let alarms: Alarm[] =  <Alarm[]>(<FormArray>this.formWarrantyAccount.controls['alarms']).getRawValue();
    fieldValue = FormatAlarm.onFocusAlarmField(alarms[position].alOn);  
    alarms[position].alOn = fieldValue;
  
    this.formWarrantyAccount.controls['alarms'].setValue(alarms); 
  }

  onFocusAlarmOffField(position: number) {
    let fieldValue : string = "";
    let alarms: Alarm[] =  <Alarm[]>(<FormArray>this.formWarrantyAccount.controls['alarms']).getRawValue();
    fieldValue = FormatAlarm.onFocusAlarmField(alarms[position].alOff);  
    alarms[position].alOff = fieldValue;
  
    this.formWarrantyAccount.controls['alarms'].setValue(alarms); 
  }

  onBlurAlarmOffField(position: number) {
   
    let fieldValue : string = "";
    let alarms: Alarm[] =  <Alarm[]>(<FormArray>this.formWarrantyAccount.controls['alarms']).getRawValue();
    fieldValue = FormatAlarm.onBlurAlarmField(alarms[position].alOff);  
    alarms[position].alOff = fieldValue;
  
    this.formWarrantyAccount.controls['alarms'].setValue(alarms); 
  }


  formatDataSend(warrantyAccount:  WarrantyAccount) : WarrantyAccount {

    //Format ID Warranty
    while (this.warrantyAccount.id.length < ID_WARRANTY_SIZE) {
      this.warrantyAccount.id = "0" + this.warrantyAccount.id;
    }

    //Format Warranty Amount
    this.warrantyAccount.warrantyAmount = this.warrantyAccount.warrantyAmount.replace(",","")
    while((this.warrantyAccount.warrantyAmount.indexOf(".")>0)){
      this.warrantyAccount.warrantyAmount = this.warrantyAccount.warrantyAmount.replace(".","");
    }
    while (this.warrantyAccount.warrantyAmount.length < WARRANTY_AMOUNT_SIZE){
      this.warrantyAccount.warrantyAmount = "0" + this.warrantyAccount.warrantyAmount;
    }

    //Format Retainer
    this.warrantyAccount.retainer = this.warrantyAccount.retainer.replace(",","")
    while((this.warrantyAccount.retainer.indexOf(".")>0)){
      this.warrantyAccount.retainer = this.warrantyAccount.retainer.replace(".","");
    }
    while (this.warrantyAccount.retainer.length < RETAINER_SIZE){
      this.warrantyAccount.retainer = "0" + this.warrantyAccount.retainer;
    }

    //Format Alarms
    this.warrantyAccount.alarms = this.formatAlarmsDataSend(this.warrantyAccount.alarms);

    return warrantyAccount;
  }

}

const ID_WARRANTY_SIZE = 8;
const RETAINER_SIZE = 12;
const ALARM_SIZE = 5;
const MAX_DECIMAL = 2;
const WARRANTY_AMOUNT_SIZE = 12;

export interface AccountType {
  accountTypeCode: string;
  accountTypeViewValue: string;
}
