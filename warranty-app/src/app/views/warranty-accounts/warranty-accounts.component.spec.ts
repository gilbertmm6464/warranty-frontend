import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarrantyAccountsComponent } from './warranty-accounts.component';

describe('WarrantyAccountsComponent', () => {
  let component: WarrantyAccountsComponent;
  let fixture: ComponentFixture<WarrantyAccountsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarrantyAccountsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarrantyAccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
