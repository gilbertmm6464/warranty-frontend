import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy, Inject, ViewContainerRef, ComponentRef} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { LoginComponent } from '../login/login.component';
import { Router } from '@angular/router';
import { SelectionModel } from '@angular/cdk/collections';
import { WarrantyAccountService } from 'src/app/services/warranty-account.service';
import { WarrantyAccount } from 'src/app/models/warranty-account';
import { WarrantyAccountList } from 'src/app/models/warranty-account-list';
import { FormatAlarm } from 'src/app/utility/format/format-alarm';
import { Alarm } from 'src/app/models/alarm';
import { FormatAmmount } from 'src/app/utility/format/format-ammount';



@Component({
  selector: 'app-warranty-accounts',
  templateUrl: './warranty-accounts.component.html',
  styleUrls: ['./warranty-accounts.component.css']
})
export class WarrantyAccountsComponent implements OnInit {

  displayedColumns: string[] = [ 'select', 'id', 'garantia', 'anticipo', 'al1On', 'al1Off', 'al2On', 
  'al2Off', 'al3On', 'al3Off', 'al4On', 'al4Off', 'al5On', 'al5Off', 'al6On', 'al6Off', 'tipo'];
  dataSource = new MatTableDataSource<WarrantyAccount>();
  selection = new SelectionModel<WarrantyAccount>(true, []);
  visibleActionItems: boolean = false;

/*
  private loginComponent: ComponentRef<LoginComponent> = null;
*/
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    /* Consult Warranty Account Service - Get all warranty accounts */
    this.warrantyAccountService.getWarrantyAccounts('N', '00000000')
          .subscribe((data:WarrantyAccountList)=>{
      
      data.accounts.forEach((element:WarrantyAccount) => {

        element.alarms.forEach((alarm: Alarm)=> {
        
          alarm.alOff = FormatAlarm.onFocusAlarmField(alarm.alOff);  
          alarm.alOff = FormatAlarm.onBlurAlarmField(alarm.alOff);  
          alarm.alOn = FormatAlarm.onFocusAlarmField(alarm.alOn);  
          alarm.alOn = FormatAlarm.onBlurAlarmField(alarm.alOn); 

        });
        
        element.warrantyAmount = element.warrantyAmount.slice(0,10)+","+ element.warrantyAmount.slice(10,12);
        element.warrantyAmount = FormatAmmount.onFocusAmmountField(element.warrantyAmount.toString());
        element.warrantyAmount = FormatAmmount.onBlurAmmountField(element.warrantyAmount.toString()).toString();
        

        element.retainer = element.retainer.slice(0,10)+","+ element.retainer.slice(10,12);
        element.retainer = FormatAmmount.onFocusAmmountField(element.retainer.toString());
        element.retainer = FormatAmmount.onBlurAmmountField(element.retainer.toString()).toString();
        

        this.dataSource.data.push(element);
      });

      console.log("Data"+JSON.stringify(this.dataSource.data));
      this.refreshTable();
    });
  }

  refreshTable(){
    /* TODO - no conforme */
    this.dataSource.filter = ' ';
    this.dataSource.filter = '';
  }

/*
  ngAfterViewInit(): void {
    let factory = this.componentFactoryResolver.resolveComponentFactory(this.vmComp);
    this.ngOnDestroy();
    this.loginComponent = this.viewContainer.createComponent(factory, null, this.viewContainer.injector);
    this.loginComponent.changeDetectorRef.detectChanges();
  }
  
  ngOnDestroy(): void {
    if (this.loginComponent) {
      this.loginComponent.changeDetectorRef.detach();
      //this.validationMessageComponent.destroy();
    }
  }
*/
  constructor(public dialog: MatDialog,
    private router: Router,
    private warrantyAccountService : WarrantyAccountService,
    private snackBar: MatSnackBar,
/*    private viewContainer: ViewContainerRef,
    private componentFactoryResolver: ComponentFactoryResolver,
    @Inject(LoginComponent) private vmComp: Type<LoginComponent>
*/    
  ) {


  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data != null ? this.dataSource.data.length: 0;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: WarrantyAccount): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }



  addWarrantyAccount(){
    this.router.navigate(['warranty-accounts/new/']);
  }

  editWarrantyAccount(){
    console.log('edit');
    let accion = "Aceptar";

    if (this.selection.selected.length > 0) {

      this.router.navigate(['warranty-accounts/edit/', (<WarrantyAccount[]>this.selection.selected)[0].id]);

    } else {

      this.snackBar.open("Debe seleccionar una garantia", accion, {
        duration: 3000,
      });

    }

  }

}