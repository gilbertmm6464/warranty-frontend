import { Component, OnInit } from '@angular/core';
import { AuthenticateService } from 'src/app/client/services/authenticate.service';
import { Router } from '@angular/router';
import { NavigationLoader } from 'src/app/components/loaders/navigation-loader.service';

@Component({
  selector: 'app-navegacion',
  templateUrl: './navegacion.component.html',
  styleUrls: ['./navegacion.component.css']
})
export class NavegacionComponent implements OnInit {

  constructor( private authenticateUserService: AuthenticateService,
    private route: Router,
    public navLoader: NavigationLoader/*,
    public responsive: ResponsiveService,*/
    ) { }

  ngOnInit(
  ) {
  }

  /**
   * Desologuea al usuario.
   */
  logout() {
    
    this.authenticateUserService.logout();
    this.route.navigate(['/login']);
    
  }


}
