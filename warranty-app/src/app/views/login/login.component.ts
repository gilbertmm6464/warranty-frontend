import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Loader, Notifiable } from '../../core/models/loader.models';

import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { AuthenticateService } from 'src/app/client/services/authenticate.service';
import { UserAuthenticate } from 'src/app/client/services/models/user-authenticate.model';
import { PoolNotificationService } from 'src/app/core/services/notification.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements Notifiable, OnInit {

  /* Loader a ser utilizado para las operaciones de backend */
  loginLoader: Loader = new Loader().notifyTo(this);

  /* Engloba los datos necesarios para login */
  private loginForm: FormGroup;

  /* El nombre del usuario */
  usernameControl: FormControl;

  /*La contraseña del usuario */
  passwordControl: FormControl;

  /* El indice del paso actual en el formulario de login */
  stepIndex = 0;

  /**
   * La url a la que redigiré el login.
   */
  private returnUrl: string;



  constructor(
    private formBuilder: FormBuilder, 
    private router: Router,
    private currentRoute: ActivatedRoute,
    private userAuthenticateService: AuthenticateService,
    private notifier: PoolNotificationService) { 

    this.usernameControl = this.formBuilder.control('', [Validators.required]);
    this.passwordControl = this.formBuilder.control('', [Validators.required]);
    this.loginForm = this.formBuilder.group({
      username: this.usernameControl,
      password: this.passwordControl
    });

  }

  ngOnInit(): void {
    const queryParams = { ...this.currentRoute.snapshot.queryParams };
    const { expired, message, returnUrl } = queryParams;
    this.returnUrl = returnUrl;
    if (expired) {
      this.notifier.unauthorized(message ? { message } : undefined);
      queryParams['expired'] = null;
      queryParams['message'] = null;
      this.router.navigate(['.'], {
        relativeTo: this.currentRoute,
        queryParams
      });
    }
  }

  /**
   * Realiza el login.
   */
  login() {
    console.log(this.loginForm);
    console.log(this.username);
    console.log(this.password);

    if (this.loginForm.valid) {

      let userAuthenticate: UserAuthenticate = {password: this.password, user: this.username};
      /*
      const userInfo: IniciarSesionRequest = { password: this.password };
      if (this.username.includes('@')) {
        userInfo.mail = this.username;
      } else {
        userInfo.nombreUsuario = this.username;
      }
      */
      this.loginLoader.load(this.userAuthenticateService.LoginAuthenticate(userAuthenticate)).subscribe(
        () => {
          /*
      this.userAuthenticateService.LoginAuthenticate(userAuthenticate).subscribe(
            res => {
                console.log(res);

                this.router.navigate([this.returnUrl || '/']);
              },
              err => {
                console.log("Error occured");
              }
      );
         */ 
          this.router.navigate([this.returnUrl || '/']);
        },
        (error: HttpErrorResponse) => {
          this.notifier.unauthorized({message: 'Contraseña o Usuario incorrecto'});
          this.resume();
        }
      );
      
    }
  }

  /**
   * Devuelve true si el paso actual es igual a `step`
   * @param step el paso que quiero saber.
   */
  isInStep(step: number): boolean {
    return this.stepIndex === step;
  }

  /**
   * Obtiene el nombre del usuario.
   */
  get username(): string {
    return this.usernameControl && this.usernameControl.value;
  }

  /**
   * Obtiene la contraseña.
   */
  get password(): string {
    return this.passwordControl && this.passwordControl.value;
  }

    /**
   * Al ser notificado por `loaderForm`, deshabilito el formulario.
   */
    wait() {
      this.loginForm.disable();
    }
    /**
     * Al resumir la carga, habilito el formulario nuevamente.
     */
    resume() {
      this.loginForm.enable();
    }


}
