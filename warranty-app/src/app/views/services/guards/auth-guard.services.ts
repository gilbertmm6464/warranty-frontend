
import { 
    CanActivateChild, 
    CanActivate, 
    ActivatedRouteSnapshot, 
    RouterStateSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthenticateService } from 'src/app/client/services/authenticate.service';
import { PoolNotificationService } from 'src/app/core/services/notification.service';
 
  /**
   * Interceptor de rutas para identificar si el usuario actual es válido.
   */
  @Injectable()
  export class AuthGuardService implements CanActivate, CanActivateChild {
    constructor(
      private authenticateUserService: AuthenticateService,
      private notifier: PoolNotificationService
    ) {}

    /**
     * Solo da la autorizacion si el usuario esta logeado.
     */
    canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
      | Observable<boolean>
      | Promise<boolean>
      | boolean {
      
      const hasCurrentSession = this.authenticateUserService.currentSession != null;
      if (!hasCurrentSession) {
        this.notifier.unauthorized();
      }
      return hasCurrentSession;
    }

    /**
     * Solo da la autorizacion si el usuario esta logeado.
     */
    canActivateChild(
      childRoute: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
    ):
      | Observable<boolean>
      | Promise<boolean>
      | boolean {
        
      return this.canActivate(childRoute, state);
    }

  }
 