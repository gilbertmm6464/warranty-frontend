
import { MatSnackBar, MatSnackBarRef } from '@angular/material/snack-bar';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationModel } from 'src/app/core/services/models/notification.model';
import { NotifierService } from 'src/app/core/services/notification.service';

/**
 * Implementacion del servicio de mensajes, para que muestre carteles de error.
 */
@Injectable()
export class SnackMessagesService implements NotifierService {
  constructor(private _snackBar: MatSnackBar, private _router: Router) {}
  send(notification: NotificationModel) {

    const snack: MatSnackBarRef<any> = this._snackBar.open(
      notification.message,
      'cerrar',
      {
        duration: 4000
      }
    );

    snack.onAction().subscribe(() => {
      snack.dismiss();
    });
    
  }

  error(notification: NotificationModel) {
    this.send(notification);
  }

  unauthorized(
    notification: NotificationModel = {
      message: 'La sesión no es válida, vuelve a iniciar sesión.'
    }
  ) {
    if (!this._router.isActive('login', false)) {
      this._router.navigate(['login'], {
        queryParams: {
          returnUrl: this._router.url,
          expired: true,
          message: notification.message
        }
      });
      return;
    } else {
      this.send(notification);
    }
  }
}
