import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

const routes: Routes = [];



/**
 * Módulo para definir todas las rutas de la aplicación.
 */
@NgModule({
  imports: [RouterModule.forRoot(routes),
            BrowserAnimationsModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }

