import { Subscriber, concat, of, Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

/**
 * Interfaz  que identifica a un objeto como `Notifiable`,
 * lo que sugiere que puede recibir notificaciones por parte
 * de un `Loader` (Cuando comienza la carga notifica `wait` y cuando termina `resume`.)
 */
export interface Notifiable {
  wait();
  resume();
}

/**
 * Las instancias de `LoaderCache` son `usadas` por un `Loader` para
 * no solo cargar observables y mantener el estado de la llamada, si no también
 * para guardar la primer respuesta según una llave (`key`).
 */
export class LoaderCache {
  /**
   * Este simple objeto es la cache, para cada key se guardará una determinada respuesta.
   */
  private _cache: { [key: string]: any } = {};

  constructor(private _loader: Loader) { }

  /**
   * Carga el observable si y solo si no existe un elemento en el objeto cache con el mismo
   * nombre que la key pasada por parámetro.
   * @param observable esto es una función, que deberá devolver el observable a llamar y sólo se registra si la key no está en el cache
   */
  load<T>(key: string, observable: () => Observable<T>): Observable<T> {
    if (!this._cache[key]) {
      return this._loader.load<T>(observable()).pipe(
        tap(value => {
          this.set(key, value);
          this.clear('');
        })
      );
    }
    return of(this._cache[key]);
  }

  /**
   * Limpia la cache según el nombre pasado por parámetro.
   * Si no se pasa un nombre (`key`), se limpiará toda la cache.
   * @param key el nombre de la cache que quiero limpiar.
   */
  clear(key?: string) {
    if (!key) {
      this._cache = {};
    } else {
      this._cache[key] = undefined;
      try {
        delete this._cache[key];
      } catch { }
    }
  }

  /**
   * Obtiene el objeto guardado para el nombre de cache (`key`)
   * pasado por parámetro.
   * @param key el nombre del objeto en cache que quiero obtener.
   */
  get<T>(key: string): T {
    return this._cache[key];
  }

  /**
   * Asigna un valor (`value`) con el nombre de cache (`key`)
   * pasado por parámetro.
   * @param key el nombre de la cache que quiero asignar.
   * @param value el valor que quiero guardar.
   */
  set(key: string, value: any) {
    this._cache[key] = value;
  }

  /**
   * Método para preguntar si hay un objecto asignado a ese nombre de cache (`key`).
   * @param key el nombre de la cache del cual quiero saber su existencia.
   */
  has(key): boolean {
    return this._cache[key] != null;
  }
}

/**
 * Utilidad para realizar multiples llamadas asincrónicas.
 *
 * Un Loader podrá registrar Observables y notificará a un objeto Notifiable 2 eventos 'wait' y 'resume'.
 * También indicará si hay alguna llamada en espera mediante el método 'isLoading'.
 * Ejecutará la función 'wait' cuando se registre el primer observable.
 * Ejecutará la función 'resume' cuando todos los observables que se hayan registrado hayan finalizado.
 *
 * Para registrar un observable se usa la función 'load'.
 *
 * ```
 const loader = new Loader().notifyTo({
      wait: ()=>console.log('now I\'m waiting...'),
      resume: ()=>console.log('now I resume...')}
 );

 const observable = new Observable((observer)=>{
      observer.next("first observable");
      setTimeout(()=>{observer.complete()},3000); //Wait for 3 seconds
 });

 console.log(loader.isLoading())
 // > false

 loader.load(observable).subscribe((nextValue)=>console.log(nextValue));
 console.log("waiting: " , loader.isLoading());
 // > now I'm waiting...
 // > first observable
 // > waiting: true
 // After 3 seconds:
 // > now I resume...
 console.log("waiting: " , loader.isLoading());
 // > waiting: false
 ```
 */
export class Loader {
  /**
   * Objeto que guarda un registro y estado de todas las llamadas a observables
   * que se realicen con éste loader.
   */
  private _registrations: {
    [key: string]: { loading: boolean; error: boolean };
  } = {};
  /**
   * Objeto a notificar, que enviará las distintas notificaciones a todos
   * los notificadores registrados.
   */
  private _notifiable: Notifiable = {
    wait: () => {
      this.notifiables.forEach((notifier: Notifiable) => {
        notifier.wait();
      });
    },
    resume: () => {
      this.notifiables.forEach((notifier: Notifiable) => {
        notifier.resume();
      });
    }
  };
  /**
   * Objeto al que se notificará (si fué definido) que tiene que esperar (`wait`) o retomar (`resume`).
   */
  readonly notifiables: Notifiable[] = [];
  /**
   * Objeto que servirá de cache y sera instanciado al ser requerido.
   */
  private _cache: LoaderCache;
  /**
   * Memoria cache de este loader, usar para cargar observables cuya respuesta quiero guardar en cache.
   */
  get cache(): LoaderCache {
    if (!this._cache) {
      this._cache = new LoaderCache(this);
    }
    return this._cache;
  }

  /**
   * Funcionalidad para marcar un token a estado cargando.
   * Si es la primer llamada, se notificará el `wait`.
   * @param token llave a ser marcada como "cargando" (`loading`)
   */
  private _wait(token: string) {
    const isNotLoading = !this.isLoading(); // Guardo la respuesta de `isLoading` ya que
    this._registrations[token].loading = true; // de otra manera, devolvería siempre true a partir de acá.
    if (isNotLoading) {
      this._clearErrors();
      this._notifiable.wait();
    }
  }

  /**
   * Funcionalidad para marcar un token como finalizado correctamente.
   * Si es la última llamada, se notificará el `resume`.
   * @param token llave a ser anulada del registro.
   */
  private _resume(token: string) {
    if (this._isLoading(token)) {
      if (!this._registrations[token].error) {
        this._unRegister(token);
      } else {
        this._registrations[token].loading = false;
      }
      // Si después de anular el registro del token, no se está
      // cargando mas nada, significa que ese fué el último token cargandose
      // por lo que notifico el `resume`.
      if (!this.isLoading()) {
        this._notifiable.resume();
      }
    }
  }

  /**
   * Funcionalidad para saber si un token esta siendo cargado.
   * @param token la llave que quiero saber si esta siendo cargada.
   */
  private _isLoading(token: string): boolean {
    return (
      this._registrations[token] != null &&
      this._registrations[token].loading === true
    );
  }

  /**
   * Anula la validez del token pasado por parámetro en el registro de observables.
   * @param token llave a ser anulada del registro de observables.
   */
  private _unRegister(token: string) {
    try {
      this._registrations[token] = undefined;
      delete this._registrations[token];
    } catch (err) {
      if (this._registrations[token] != null) {
        throw err;
      }
    }
  }

  /**
   * Crea un nuevo token y lo registra en el objecto de registros
   * de observables (`_registrations`).
   * @returns el token generado.
   */
  private _register(): string {
    const token = new Date().getTime() + '' + Math.random();
    if (token in this._registrations) {
      return this._register();
    }
    this._registrations[token] = {
      loading: false,
      error: false
    };
    return token;
  }

  /**
   * Limpia todos las registraciones en estado de error.
   */
  private _clearErrors() {
    Object.keys(this._registrations).forEach(token => {
      if (this._registrations[token].error) {
        this._unRegister(token);
      }
    });
  }

  /**
   * Asigna un objeto `Notifiable`.
   * Este objeto será notificado ante los eventos de `wait` y `resume` del `Loader`.
   * @param notifiable el objeto al que el loader enviará notificaciones.
   */
  notifyTo(notifiable: Notifiable): this {
    this.notifiables.push(notifiable);
    return this;
  }

  /**
   * Devuelve `true` si alguno de todos los observables registrados
   * esta siendo cargado.
   */
  isLoading(): boolean {
    return Object.keys(this._registrations).some(token =>
      this._isLoading(token)
    );
  }

  /**
   * Devuelve `true` si alguno de todos los observables registrados
   * registró algún error en su respuesta.
   */
  hasError(): boolean {
    return Object.keys(this._registrations).some(
      token => this._registrations[token].error
    );
  }

  /**
   * Limpia todos los registros de observables (incluyendo los que estan en estado de error)
   * y notifica el evento `resume` de ser necesario.
   */
  clear(): this {
    const isLoading = this.isLoading();
    this._registrations = {};
    if (isLoading) {
      this._notifiable.resume();
    }
    return this;
  }

  /**
   * Registra un objeto observable. Si es el primero, se ejecutará la función 'wait' del
   * objeto Notifiable (si se registró alguno). Una vez que todos y cada uno de los observables terminen
   * de ejecutarse, se llamará a la función 'resume' del objeto Notifiable (si se registró alguno).
   *
   */
  load<T>(observable: Observable<T>): Observable<T> {
    const token = this._register();
    return concat(
      new Observable((subscriber: Subscriber<any>) => {
        this._wait(token);
        subscriber.complete();
      }),
      observable
    ).pipe(
      tap(() => {
        this._resume(token);
      }),
      catchError(err => {
        this._registrations[token].error = true;
        this._resume(token);
        throw err;
      })
    );
  }
}
