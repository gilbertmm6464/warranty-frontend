import { InjectionToken, Injectable, Inject } from '@angular/core';
import { NotificationModel } from './models/notification.model';

/**
 * Interfaz de servicio que manejará las distintas notificaciones que tengan que ser atendidos por la aplicación.
 */
export interface NotifierService {
  /**
   * Método para recibir un mensaje a ser tratado.
   */
  send(notification: NotificationModel);
  /**
   * Método para recibir un mensaje de error a ser tratado.
   */
  error(notification: NotificationModel);
  /**
   * Método para recibir notificación de que el usuario no fue autorizado.
   */
  unauthorized(notification?: NotificationModel);
}

/**
 * Token para injectar servicios de notificaciones, usar con el parámtro `multi`.
 */
export const NOTIFIER_SERVICES: InjectionToken<
  NotifierService[]
> = new InjectionToken('NOTIFIER_SERVICES');

/**
 * Servicio que recibe todas las notificaciones y las despacha a los servicios de notificaciones
 * que fueron injectados mediante el token `NOTIFIER_SERVICES`.
 */
@Injectable()
export class PoolNotificationService implements NotifierService {
  constructor(
    @Inject(NOTIFIER_SERVICES) private _notifiers: NotifierService[]
  ) {}
  /**
   * Método para despachar dinámicamente todos los mensajes al método correspondiente.
   */
  protected _dispatchNotification(
    notificationType: string,
    notification?: NotificationModel
  ) {
    if (this._notifiers.length > 0) {
      this._notifiers.forEach((service: NotifierService) => {
        const method = service[notificationType];
        if (typeof method === 'function') {
          const args = [];
          if (notification) {
            args.push(notification);
          }
          method.apply(service, args);
        }
      });
    }
  }
  send(notification: NotificationModel) {
    this._dispatchNotification('send', notification);
  }
  error(notification: NotificationModel) {
    this._dispatchNotification('error', notification);
  }
  unauthorized(notification?: NotificationModel) {
    this._dispatchNotification('unauthorized', notification);
  }
}

/**
 * Servicio de notificaciones por defecto, que no realiza operación alguna.
 * Redefinir si se quiere incluir un único servicio para toda la aplicación
 * y a su vez es el primer servicio que recibe cada notificación.
 */
@Injectable()
export class DefaultNotifierService implements NotifierService {
  send(notification: NotificationModel) {}
  error(notification: NotificationModel) {}
  unauthorized(notification?: NotificationModel) {}
}
