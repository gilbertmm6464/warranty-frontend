/**
 * Modelo de una notificación.
 */
export class NotificationModel {
    /**
     * Por defecto, este modelo solo contempla recibir mensajes.
     */
    message: string;
  }
  