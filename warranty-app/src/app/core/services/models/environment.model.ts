/**
 * Modelo predefinido para las variables de entorno de la aplicación.
 */
export class EnvironmentModel {
    /**
     * Indica si la aplicación fue construida en su version productiva.
     */
    production: boolean;
    /**
     * La url base del backend.
     */
    apiUrl: string;
  }
  