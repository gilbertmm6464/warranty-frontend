import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { WarrantyAccount } from '../models/warranty-account';
import { WarrantyAccountList } from '../models/warranty-account-list';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable()
export class WarrantyAccountService {

  url : string = 'http://localhost:8080/warranties/accounts/';
  // headers: HttpHeaders = new HttpHeaders();
  
  params : HttpParams = new HttpParams();

  constructor(private http: HttpClient) { 

    // Set Headers 
    // this.headers.append('Content-Type', 'application/json');

  }

  // Get Warranty Account by id
  getWarrantyAccount(id: string): Observable<WarrantyAccount> {
    
    return this.http.get<WarrantyAccount>(this.url.concat(id));

  }

  // Get Warranty Accounts by List 30
  getWarrantyAccounts(aprobada: string, idGarantia:string): Observable<WarrantyAccountList> {

    return this.http.get<WarrantyAccountList>(this.url, 
                        {headers: new HttpHeaders({'Content-Type':  'application/json'}),
                        params : new HttpParams().set('aprobada',aprobada)
                                .set('idGarantia',idGarantia)
                                });
  }

  // Create Warranty Account
  createWarrantyAccount(warrantyAccount: WarrantyAccount): Observable<WarrantyAccount> {
  
    console.log("createWarrantyAccount");
    
    return this.http.post<WarrantyAccount>(this.url, 
        JSON.stringify(warrantyAccount), httpOptions);
  }

  // Update Warranty Account
  updateWarrantyAccount(warrantyAccount: WarrantyAccount): Observable<WarrantyAccount> {
  
    /* Testear */
    console.log("updateWarrantyAccount");

    return this.http.put<WarrantyAccount>(this.url, 
        JSON.stringify(warrantyAccount), httpOptions);

  }

  // Delete Warranty Account
  deleteWarrantyAccount(aprobada: string, idGarantia:string): Observable<WarrantyAccount> {
  
    /* Testear */
    console.log("deleteWarrantyAccount");

    return this.http.delete<WarrantyAccount>(this.url, 
      {headers: new HttpHeaders({'Content-Type':  'application/json'}),
      params : new HttpParams().set('aprobada',aprobada)
              .set('idGarantia',idGarantia)
              });
  }


}
