import { TestBed } from '@angular/core/testing';
import { WarrantyAccountService } from './warranty-account.service';

describe('WarrantyAccountService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WarrantyAccountService = TestBed.get(WarrantyAccountService);
    expect(service).toBeTruthy();
  });
});
